import * as crypto from 'crypto';


export function hash(input, algo = 'SHA256') {
	if (typeof input !== 'string' || typeof algo !== 'string')
		throw new Error(`input and algo passed to hash(), both need to be strings! (got "${typeof input}" and "${typeof algo}")`);
	const hash = crypto.createHash(algo);
	hash.update(input).end();
	return hash.digest('hex');
}

export function clone (obj) {
	return JSON.parse(JSON.stringify(obj));
}
